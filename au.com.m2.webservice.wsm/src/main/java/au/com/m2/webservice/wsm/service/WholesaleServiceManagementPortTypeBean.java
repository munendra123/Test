package au.com.m2.webservice.wsm.service;

import au.com.m2.webservice.wsm.adsl.service.ADSLSEI;
import au.com.m2.webservice.wsm.common.exception.ErrorCode;
import au.com.m2.webservice.wsm.common.exception.FaultResponse;
import au.com.m2.webservice.wsm.common.interceptors.ValidateAccessInterceptor;
import au.com.m2.webservice.wsm.common.service.AccessManagement;
import au.com.m2.webservice.wsm.common.service.GenericPortType;
import au.com.m2.webservice.wsm.common.status.interceptor.ProcessProductIdInterceptor;
import au.com.m2.webservice.wsm.common.status.model.TransactionStatus;
import au.com.m2.webservice.wsm.common.status.service.TCAServiceSEI;
import au.com.m2.webservice.wsm.common.util.Constants;
import au.com.m2.webservice.wsm.common.util.Constants.PROFILE;
import au.com.m2.webservice.wsm.common.util.Constants.Product;
import au.com.m2.webservice.wsm.common.util.TransactionIdGenerator;
import au.com.m2.webservice.wsm.common.wholesaleservicemanagement.*;
import au.com.m2.webservice.wsm.cpe.service.CPESEI;
import au.com.m2.webservice.wsm.dir.service.DirectorySEI;
import au.com.m2.webservice.wsm.email.service.EmailSEI;
import au.com.m2.webservice.wsm.interceptor.StartCallBackTimerInterceptor;
import au.com.m2.webservice.wsm.mob.service.MOBSEI;
import au.com.m2.webservice.wsm.admin.service.AdminSEI;
import au.com.m2.webservice.wsm.oper.service.OperSEI;
import au.com.m2.webservice.wsm.fibre.service.FibreSEI;
import au.com.m2.webservice.wsm.voip.service.VoipSEI;
import au.com.m2.webservice.wsm.sq.service.SqSEI;
import au.com.m2.webservice.wsm.webhost.service.WebhostSEI;

import org.apache.commons.lang.StringUtils;
import org.jboss.ws.annotation.SchemaValidation;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.jws.HandlerChain;
import javax.jws.WebService;

/**
 * Bean implementation of the WSM web service interface.
 * 
 * @author vpulla
 */
@WebService(serviceName = "WholesaleServiceManagement", endpointInterface = "au.com.m2.webservice.wsm.service.WholesaleServiceManagementPortType", portName = "WholesaleServiceManagementPort", targetNamespace = "https://wsm.webservice.m2.com.au/WholesaleServiceManagement/", wsdlLocation = "META-INF/wsdl/WholesaleServiceManagement.wsdl")
@Stateless(name = "WholesaleServiceManagement")
@HandlerChain(file = "TCASSOAPMessageHandler.xml")
@SchemaValidation(schemaLocation = "META-INF/wsdl/WholesaleServiceManagement.xsd")
@Interceptors( { ProcessProductIdInterceptor.class, ValidateAccessInterceptor.class, StartCallBackTimerInterceptor.class })
public class WholesaleServiceManagementPortTypeBean implements WholesaleServiceManagementPortType {

    // @EJB(name="DirectoryWS")
    // @EJB(mappedName = "DirectoryWS/remote")
    @EJB
    private DirectorySEI directorySEI;
    /**
     * OWCService reference.
     */

    // @EJB(name = "OWCService")
    // @EJB(mappedName = "OWCServiceWS/remote")

    /***
     * Comment out by Ryan LI as decommissioned
     */
    // @EJB
    // private OWCSEI owcSEI;

    // @EJB(mappedName = "MOBServiceWS/remote")
    @EJB
    private MOBSEI mobSEI;

    // @EJB(name = "TCAServiceWS")
    // @EJB(name = "TCAServiceWS", mappedName = "TCAServiceWS/remote")
    @EJB
    private TCAServiceSEI tcasServiceSEI;

    /***
     * Comment out by Ryan LI as decommissioned
     */
    // @EJB(mappedName = "ITILServiceWS/remote")
    // private ITILSEI itilSEI;

    @EJB(mappedName = "ADSLServiceWS/remote")
    private ADSLSEI adslsei;

    @EJB
    private EmailSEI emailsei;

    @EJB
    private OperSEI opersei;

    // @EJB(mappedName = "WebhostSEI/remote")
    @EJB
    private WebhostSEI webhostsei;
    
    @EJB
    private CPESEI cpesei;
    
    @EJB
    private SqSEI sqSei;

    @EJB
    private FibreSEI fibreSei;
    
    @EJB
    private VoipSEI voipSei;
    
    @EJB
    private AdminSEI adminSei;
    
    private AccessManagement accessManagement;

    // @EJB(name = "AccessManagement", mappedName = "AccessManagement/remote")
    @EJB
    public void setAccessManagement(AccessManagement accessManagement) {
        System.out.println("\n\n\nWSMCommonPortTypeBean.setAccessManagement()");
        this.accessManagement = accessManagement;
    }

    /**
     * Transaction suffix constant.
     */
    public static final String TRANSACTION_SUFFIX = "WSM";
    // private Map<String, CreateRequest> services = new HashMap<String,
    // CreateRequest>();

//    WholesaleServiceManagementSEISandbox sandbox;
    
    private GenericPortType getSEI(String productId) throws FaultResponse {
    	
    	// if (Product.OWC.equals(productId)) {
        // return owcSEI;
        // } else
        if (Product.DIR.equals(productId)) {
            return directorySEI;
        } else if (Product.TCAS.equals(productId)) {
            return tcasServiceSEI;
        } else if (Product.MOB.equals(productId)) {
            return mobSEI;
        }
        // else if (Product.ITIL.equals(productId)) {
        // return itilSEI;
        // }
        else if (Product.ADSL.equals(productId)) {
            return adslsei;
        } else if (Product.EMAIL.equals(productId)) {
            return emailsei;
        } else if (Product.OPER.equals(productId)) {
            return opersei;
        } else if (Product.WEBHOST.equals(productId)) {
            return webhostsei;
        } else if (Product.CPE.equals(productId)) {
            return cpesei;
        } else if (Product.SQ.equals(productId)) {
            return sqSei;
        } else if (Product.FIBRE.equals(productId)) {
            return fibreSei;
        } else if (Product.VOIP.equals(productId)) {
            return voipSei;
        } else if (Product.ADMIN.equals(productId)) {
            return adminSei;
        } else {
            throw notValidProduct(productId, TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
        }
    }

    /** {@inheritDoc} */
    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public DefaultResponse create(final CreateRequest createRequest) throws FaultResponse {
        if (Constants.DEBUG_MODE) {
            System.out.println("*** " + createRequest.getProductID());
            System.out.println("Create");
        }

        DefaultResponse response = null;

        try {

            // if (Constants.SANDBOX_MODE) {
            // response = getSandbox().create(createRequest);
            // } else {

            String productId = createRequest.getProductID();
            if (productId == null || productId.length() == 0) {
                throw notValidProduct("Product id is mandatory", TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
            }
            
            createRequest.setProfile(validateProfile(createRequest.getProfile()));

            response = getSEI(productId).create(createRequest);
            
            // }
        } catch (Throwable e) {
            accessManagement.logTxStatusException(e, "create", createRequest.getAccessKey(), createRequest.getProductID(), createRequest.getPlanID(), createRequest.getScope(), null, createRequest.getParameters(), createRequest.getAliasKey());
        }
        TransactionStatus txStatus = accessManagement.logTxStatus(response.getTransactionID(), null, "create", createRequest.getAccessKey(), createRequest.getProductID(), createRequest.getPlanID(),
                createRequest.getScope(), null, createRequest.getParameters(), response, createRequest.getAliasKey());

        if (response.getTransactionID() == null) {
            response.setTransactionID(txStatus.getTxId());
        }
        return response;

    }

    /** {@inheritDoc} */
    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public DefaultResponse delete(final DeleteRequest deleteRequest) throws FaultResponse {
        if (Constants.DEBUG_MODE) {
            System.out.println("*** " + deleteRequest.getProductID());
            System.out.println("Delete");
        }

        DefaultResponse response = null;

        try {

            // if (Constants.SANDBOX_MODE) {
            // response = getSandbox().delete(deleteRequest);
            // } else {

            String productId = deleteRequest.getProductID();
            if (productId == null || productId.length() == 0) {
                throw notValidProduct("Product id is mandatory", TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
            }
            
            deleteRequest.setProfile(validateProfile(deleteRequest.getProfile()));

            response = getSEI(productId).delete(deleteRequest);
            
            // }
        } catch (Throwable e) {
            accessManagement.logTxStatusException(e, "delete", deleteRequest.getAccessKey(), deleteRequest.getProductID(), deleteRequest.getPlanID(), deleteRequest.getScope(), null, deleteRequest.getParameters(), deleteRequest.getAliasKey());
        }
        TransactionStatus txStatus = accessManagement.logTxStatus(response.getTransactionID(), null, "delete", deleteRequest.getAccessKey(), deleteRequest.getProductID(), deleteRequest.getPlanID(),
                deleteRequest.getScope(), null, deleteRequest.getParameters(), response, deleteRequest.getAliasKey());
        if (response.getTransactionID() == null) {
            response.setTransactionID(txStatus.getTxId());
        }
        return response;
    }

    /** {@inheritDoc} */
    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public DefaultResponse get(final GetRequest getRequest) throws FaultResponse {
        if (Constants.DEBUG_MODE) {
            System.out.println("*** " + getRequest.getProductID());
            System.out.println("Get");
        }

        DefaultResponse response = null;

        try {

            // if (Constants.SANDBOX_MODE) {
            // response = getSandbox().get(getRequest);
            // } else {

            String productId = getRequest.getProductID();
            if (productId == null || productId.length() == 0) {
                throw notValidProduct("Product id is mandatory", TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
            }
            
            getRequest.setProfile(validateProfile(getRequest.getProfile()));

            response = getSEI(productId).get(getRequest);
            
            // }
        } catch (Throwable e) {
            accessManagement.logTxStatusException(e, "get", getRequest.getAccessKey(), getRequest.getProductID(), getRequest.getPlanID(), getRequest.getScope(), null, getRequest.getParameters(), getRequest.getAliasKey());
        }
        TransactionStatus txStatus = accessManagement.logTxStatus(response.getTransactionID(), null, "get", getRequest.getAccessKey(), getRequest.getProductID(), getRequest.getPlanID(), getRequest
                .getScope(), null, getRequest.getParameters(), response, getRequest.getAliasKey());
        if (response.getTransactionID() == null) {
            response.setTransactionID(txStatus.getTxId());
        }
        return response;
    }

    /** {@inheritDoc} */
    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public DefaultResponse set(final SetRequest setRequest) throws FaultResponse {
        if (Constants.DEBUG_MODE) {
            System.out.println("*** set " + setRequest.getProductID());
        }

        DefaultResponse response = null;

        try {

            // if (Constants.SANDBOX_MODE) {
            // response = getSandbox().set(setRequest);
            // } else {

            String productId = setRequest.getProductID();
            if (productId == null || productId.length() == 0) {
                throw notValidProduct("Product id is mandatory", TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
            }
            
            setRequest.setProfile(validateProfile(setRequest.getProfile()));

            response = getSEI(productId).set(setRequest);
            
            // }
        } catch (Throwable e) {
            accessManagement.logTxStatusException(e, "set", setRequest.getAccessKey(), setRequest.getProductID(), setRequest.getPlanID(), setRequest.getScope(), null, setRequest.getParameters(), setRequest.getAliasKey());
        }
        TransactionStatus txStatus = accessManagement.logTxStatus(response.getTransactionID(), null, "set", setRequest.getAccessKey(), setRequest.getProductID(), setRequest.getPlanID(), setRequest
                .getScope(), null, setRequest.getParameters(), response, setRequest.getAliasKey());
        if (response.getTransactionID() == null) {
            response.setTransactionID(txStatus.getTxId());
        }
        return response;
    }

    /**
     * Returns a new FaultResponse object.
     * 
     * @param reason
     *            the reason
     * @param txId
     *            the transactionId
     * @return the created faultResponse object
     * @throws FaultResponse
     */
    private FaultResponse notValidProduct(final String reason, final String txId) throws FaultResponse {
        return ErrorCode.createFaultResponse(reason, ErrorCode.WSM_4221_INVALPRODUCT, txId, null);
    }


    private String validateProfile(final String profile) throws FaultResponse{
    	if(StringUtils.isBlank(profile)) {
    		return PROFILE.DEFAULT.toString();
    	}
    	
    	try {
    		return PROFILE.valueOf(profile).toString();
    	} catch (IllegalArgumentException e) {
    		throw ErrorCode.createFaultResponse(ErrorCode.createException(ErrorCode.WSM_4225_INVAL_PROFILE)
    											, TransactionIdGenerator.generateId(null, TRANSACTION_SUFFIX));
    	}

    }
}

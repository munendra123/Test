package au.com.m2.webservice.wsm.util;


/**
 * Common constants.
 * 
 * @author vpulla
 * @deprecated by anton.kattan. Use constants from common module. Refer to {@link au.com.m2.webservice.wsm.common.util.Constants.Product}
 */
@Deprecated
public final class Constants {


    /**
     * ProductID constants.
     * 
     * @author akattan
     */
    public final class Product {
        public static final String DIR = "DIR";
//        public static final String OWC = "OWC";
        public static final String MOB = "MOB";
        public static final String TCAS = "TCAS";
//        public static final String ITIL = "ITIL";
        public static final String ADSL = "ADSL";
        public static final String EMAIL = "EMAIL";
        public static final String OPER = "OPER";
        public static final String WEBHOST = "WEBHOST";
        public static final String CPE = "CPE";
    }

}

package au.com.m2.webservice.wsm.interceptor;

import static au.com.m2.webservice.wsm.common.util.Constants.PRODUCT_ID_SEPERATOR;
import static au.com.m2.webservice.wsm.common.util.Constants.WSOperation.CREATE;
import static au.com.m2.webservice.wsm.common.util.Constants.WSOperation.DELETE;
import static au.com.m2.webservice.wsm.common.util.Constants.WSOperation.GET;
import static au.com.m2.webservice.wsm.common.util.Constants.WSOperation.SET;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.commons.lang.StringUtils;

import au.com.m2.webservice.wsm.common.util.Constants;
import au.com.m2.webservice.wsm.common.wholesaleservicemanagement.CreateRequest;
import au.com.m2.webservice.wsm.common.wholesaleservicemanagement.DeleteRequest;
import au.com.m2.webservice.wsm.common.wholesaleservicemanagement.GetRequest;
import au.com.m2.webservice.wsm.common.wholesaleservicemanagement.SetRequest;

/**
 * Interceptor to process ProductID, retrieve the TransactionStatusID and save
 * it as a parameter.
 * 
 * @author vpulla
 * @deprecated use
 *             {@link au.com.m2.webservice.wsm.common.status.interceptor.ProcessProductIdInterceptor}
 */
@Deprecated
public class ProcessProductIdInterceptor {

    /**
     * Method which processes the ProductID.
     * 
     * @param ic
     *            the invocation context.
     * @return Object either proceeds to function call or
     * @throws Exception
     *             on error.
     */
    @AroundInvoke
    public Object processProductID(final InvocationContext ic) throws Exception {
        final String methodName = ic.getMethod().getName();
        final Object[] parameters = ic.getParameters();

        if (CREATE.equals(methodName)) {
            final CreateRequest createReq = (CreateRequest) parameters[0];
            this.update(createReq);
        } else if (DELETE.equals(methodName)) {
            final DeleteRequest deleteReq = (DeleteRequest) parameters[0];
            this.update(deleteReq);
        } else if (GET.equals(methodName)) {
            final GetRequest getReq = (GetRequest) parameters[0];
            this.update(getReq);
        } else if (SET.equals(methodName)) {
            final SetRequest setReq = (SetRequest) parameters[0];
            this.update(setReq);
        }

        return ic.proceed();
    }

    /**
     * Method updates the CreateRequest with correct productID and
     * transactionStatusId values based on the seperator.
     * 
     * @param createRequest
     *            the createRequest
     */
    private void update(final CreateRequest createRequest) {
        final String productID = createRequest.getProductID();
        final String[] values = StringUtils.split(productID, PRODUCT_ID_SEPERATOR);
        createRequest.setProductID(values[0]);
        if (values.length > 1)
            createRequest.getParameters().setParamValue(Constants.SOAP_BODY.PARAM_TCAS_KEY, values[1]);
    }

    /**
     * Method updates the DeleteRequest with correct productID and
     * transactionStatusId values based on the seperator.
     * 
     * @param deleteRequest
     *            the deleteRequest
     */
    private void update(final DeleteRequest deleteRequest) {
        final String productID = deleteRequest.getProductID();
        final String[] values = StringUtils.split(productID, PRODUCT_ID_SEPERATOR);
        deleteRequest.setProductID(values[0]);
        if (values.length > 1)
            deleteRequest.getParameters().setParamValue(Constants.SOAP_BODY.PARAM_TCAS_KEY, values[1]);
    }

    /**
     * Method updates the GetRequest with correct productID and
     * transactionStatusId values based on the seperator.
     * 
     * @param getRequest
     *            the getRequest
     */
    private void update(final GetRequest getRequest) {
        final String productID = getRequest.getProductID();
        final String[] values = StringUtils.split(productID, PRODUCT_ID_SEPERATOR);
        getRequest.setProductID(values[0]);
        if (values.length > 1)
            getRequest.getParameters().setParamValue(Constants.SOAP_BODY.PARAM_TCAS_KEY, values[1]);
    }

    /**
     * Method updates the SetRequest with correct productID and
     * transactionStatusId values based on the seperator.
     * 
     * @param setRequest
     *            the setRequest
     */
    private void update(final SetRequest setRequest) {
        final String productID = setRequest.getProductID();
        final String[] values = StringUtils.split(productID, PRODUCT_ID_SEPERATOR);
        setRequest.setProductID(values[0]);
        if (values.length > 1)
            setRequest.getParameters().setParamValue(Constants.SOAP_BODY.PARAM_TCAS_KEY, values[1]);
    }
}

package au.com.m2.webservice.wsm.interceptor;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import au.com.m2.webservice.wsm.common.status.service.WSMCallBack;

/**
 * Hack put in place to start WSM Call Back timer for any request that comes to
 * WSM.
 * 
 * @author vpulla
 */
public class StartCallBackTimerInterceptor {
    /**
     * WSM Call Back bean reference.
     */
    @EJB(mappedName = "WSMCallBackBean/remote")
    private WSMCallBack wsmCallBack;

    /**
     * this counter is used to regulate calling the callback. the first time wsm
     * is called and every n (frequencyOfCallback) wsm calls after that.
     */
    private static int periodicCounter = 0;
    /**
     * Every how many wsm calls before calling startTimer of the wsm callback.
     */
    private static final int frequencyOfCallback = 100;

    /**
     * Starts the WSM Call Back timer.
     * 
     * @param ic
     *            ic
     * @return {@link Object}
     * @throws Exception
     */
    @AroundInvoke
    public Object startCallBackTimer(final InvocationContext ic) throws Exception {
        System.out.println("\n\n\n\nstartCallBackTimer\n\n\n\n");
        /*
         * Sandbox mode now will send call back too. WSM-864: WSM 2.0.0 -
         * Sandbox improvement to perform SOAP call back
         */
        // if (!Constants.SANDBOX_MODE) {

        // if (periodicCounter % frequencyOfCallback == 0)
        // wsmCallBack.startTimer();
        if (periodicCounter == 0) {
            wsmCallBack.startTimer();
            periodicCounter++;
        }
        // }

        return ic.proceed();
    }

    public static void main(String[] args) {
        System.out.println(0 % frequencyOfCallback);
        System.out.println(1 % frequencyOfCallback);
        System.out.println(2 % frequencyOfCallback);
        System.out.println(100 % frequencyOfCallback);
        System.out.println(1000 % frequencyOfCallback);
        System.out.println(1001 % frequencyOfCallback);
    }
}

package au.com.m2.webservice.wsm.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-b02- Generated
 * source version: 2.0
 */
@WebServiceClient(name = "WholesaleServiceManagement", targetNamespace = "https://wsm.webservice.m2.com.au/WholesaleServiceManagement/", wsdlLocation = "https://wsm.webservice.m2.com.au/WholesaleServiceManagement/wsm?wsdl")
public class WholesaleServiceManagement extends Service {

    private final static URL WHOLESALESERVICEMANAGEMENT_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(au.com.m2.webservice.wsm.service.WholesaleServiceManagement.class.getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = au.com.m2.webservice.wsm.service.WholesaleServiceManagement.class.getResource(".");
            url = new URL(baseUrl, "https://wsm.m2.com.au/WholesaleServiceManagement/wsm?wsdl");
        } catch (MalformedURLException e) {
            logger.warning("Failed to create URL for the wsdl Location: 'https://wsm.m2.com.au/WholesaleServiceManagement/wsm?wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        WHOLESALESERVICEMANAGEMENT_WSDL_LOCATION = url;
    }

    public WholesaleServiceManagement(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WholesaleServiceManagement() {
        super(WHOLESALESERVICEMANAGEMENT_WSDL_LOCATION, new QName("https://wsm.webservice.m2.com.au/WholesaleServiceManagement/", "WholesaleServiceManagement"));
    }

    /**
     * @return returns WholesaleServiceManagementPortType
     */
    @WebEndpoint(name = "WholesaleServiceManagementPort")
    public WholesaleServiceManagementPortType getWholesaleServiceManagementPort() {
        return super.getPort(new QName("https://wsm.webservice.m2.com.au/WholesaleServiceManagement/", "WholesaleServiceManagementPort"), WholesaleServiceManagementPortType.class);
    }

}
